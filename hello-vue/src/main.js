import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

// filtros para se utilizar no Template dos compoenntes Vue
Vue.filter('capitalize', function(value){
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

// Vue.http.interceptors.push((request, next) => {
//   request.headers.set('access-control-allow-origin', '*')
//   next()
// })

// arquivo Root do Vue, ou Arquivo Raiz
new Vue({
  render: h => h(App),
}).$mount('#app')

window.Vue = Vue;